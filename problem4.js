function carYears(inventory) {
  var car_years = [];
  for (index = 0; index < inventory.length; index++) {
    car_years.push(inventory[index].car_year);
  }
  return car_years;
}
module.exports = carYears;
