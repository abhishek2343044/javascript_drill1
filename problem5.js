function OldCars(car_years) {
  var old_cars = [];
  for (index = 0; index < car_years.length; index++) {
    if (car_years[index] < 2000) {
      old_cars.push(car_years[index]);
    }
  }
  return old_cars;
}
module.exports = OldCars;
