function ListBmwAudi(inventory) {
  var BMWandAudi = [];
  for (index = 0; index < inventory.length; index++) {
    if (
      inventory[index].car_make == 'BMW' ||
      inventory[index].car_make == 'Audi'
    ) {
      BMWandAudi.push(inventory[index]);
    }
  }
  return BMWandAudi;
}

module.exports = ListBmwAudi;
