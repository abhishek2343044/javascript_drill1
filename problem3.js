function car_models(inventory) {
  var car_model_list = [];
  for (index = 0; index < inventory.length; index++) {
    car_model_list.push(inventory[index].car_model);
  }
  return car_model_list.sort();
}
module.exports = car_models;
